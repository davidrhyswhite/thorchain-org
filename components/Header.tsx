import React, { FC } from 'react'
import Link from 'next/link'
import Logo from 'assets/img/thorchain-white-text.svg'

const linkStyles = {
  color: '#fff', fontSize: '20px', margin: 10
}

export const Header: FC = () => (
  <nav id="mainNav" className="navbar navbar-expand-lg" style={{ backgroundColor: "#101921" }}>
    {/* <div className="container">
      <a className="navbar-brand" style={{ maxWidth: '40%' }}>
        <img className="top-brand" src={Logo} />
      </a>
    </div> */}
    <div className="col-md-1 col-lg-3">
      <div className="container">
        <div className="col">
          <Link href="/">
            <a>
              <img className="img-fluid" src={Logo} />
            </a>
          </Link>
        </div>
      </div>
    </div>
    <div className="col-lg-9">
      <div className="container" style={{ float: "right", textAlign: "right", marginTop:10 }}>
        <Link href="/explore">
          <a style={linkStyles}>
            EXPLORE
                  </a>
        </Link>
        <Link href="/dev">
          <a style={linkStyles}>
            DEV
                  </a>
        </Link>
        <Link href="/tech">
          <a style={linkStyles}>
            TECH
                  </a>
        </Link>
        <Link href="/about">
          <a style={linkStyles}>
            ABOUT
                  </a>
        </Link>
      </div>
    </div>
  </nav>
)

export default Header
