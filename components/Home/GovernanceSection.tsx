import React, { FC } from 'react'

export const GovernanceSection: FC = () => (
  <div className="container-fluid lgrey-bg min-h-60v pb-10vh" id="GOVERNANCE">
    <div className="container">
      <div className="row text-center  pt-10vh">
        <div className="col-12 pb-50">
          <h2 style={{ letterSpacing: '2.75px' }}>GOVERNANCE</h2>
          <div
            className="bg-thorchain-gradient-center"
            style={{ margin: '20px auto', height: '2px', width: '75%' }}
          />
          <h5>A GOVERNANCE-MINIMAL APPROACH</h5>
        </div>
      </div>
      <div className="row pt-5vh">
        <div className="col-lg-6 text-center">
          <div className="container">
            <div className="row">
              <div className="col mx-3 py-1">
                <img className="img-fluid" src={require('assets/img/governance.png')} />
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-md-1">
          <div className="container pitch-text">
            {/* <h5>PROTECTING STAKED CAPITAL AT ALL COSTS</h5> */}
            <ul>
              <li>Staked capital drives the network direction (listed assets and chains)</li>
              <li>Developers respond to the network, delivering tested and validated code</li>
              <li>Nodes choose to run backwards-compatible software</li>
              <li>Nodes are anonymous and do not communicate, coordinate, or socially-signal</li>
              <li>Nodes are second-class citizens that service the network</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default GovernanceSection
