import React, { FC } from 'react'

export const TeamSection: FC = () => (
  <div className="container-fluid min-h-60v pb-10vh" id="TEAM">
    <div className="container">
      <div className="row text-center  pt-10vh">
        <div className="col-12 pb-50">
          <h2 style={{ letterSpacing: '2.75px' }}>TEAM</h2>
          <div
            className="bg-thorchain-gradient-center"
            style={{ margin: '20px auto', height: '2px', width: '75%' }}
          />
          <h5>PSEUDO-ANONYMITY IS A STRENGTH</h5>
        </div>
      </div>
      <div className="row pt-5vh">
        <div className="col-lg-6 col-md-12">
          <div className="col-12 pitch-text">
            {/* <h5>PRINCIPLES</h5> */}
            <ul>
              <li>The team is mostly pseudo-anonymous to protect the project</li>
              <li>
                Figureheads, personalities and founders undermine a project’s ability to
                decentralise
              </li>
              <li>Transparency is demonstrated in other facets (treasury, code, research)</li>
              <li>In time, Node Operators will fund satellite development teams</li>
              <li>Reach out on Telegram, Twitter or Discord to discuss further</li>
            </ul>
          </div>
        </div>
        <div className="col-lg-6 text-center">
          <div className="container">
            <div className="row">
              <div className="col mx-3 py-1">
                <img className="img-fluid" src={require('assets/img/team.png')} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default TeamSection
