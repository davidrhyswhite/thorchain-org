import React, { FC } from 'react'

export const TechSection: FC = () => (
  <>
    <div className="container-fluid min-h-60v pt-20vh pb-10vh" id="SECURITY">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 text-center" />
          <div className="col-lg-6 col-md-12">
            <h2 className="my-1">SOLVING SECURITY</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 text-center my-5">
            <div className="container">
              <div className="row">
                <div className="col mx-3 py-1">
                  <img className="img-fluid" src={require('assets/img/bonded-staked.png')} />
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-12">
            <div className="container pitch-text py-5">
              <h5> </h5>
              <ul>
                <li>Incentives ensure bonded RUNE is always double staked RUNE</li>
                <li>Malicious nodes are slashed to protect staked capital</li>
                <li>The liquidity and security of the system is tightly coupled</li>
                <li>A Threshold Signature Scheme with no trusted dealer protects assets </li>
                <li>The system is always Byzantine fault tolerant</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid grey-bg min-h-60v pt-10vh pb-10vh" id="SCALABILITY">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-12">
            <div className="col-12 pitch-text">
              <h2 className="my-5">SOLVING SCALABILITY</h2>
              <ul>
                <li>Liquidity is sharded into realms to reduce signing committee sizes</li>
                <li>Liquidity is delegated into smaller vaults for faster signing</li>
                <li>Base infrastructure is Tendermint (100+ Nodes possible)</li>
                <li>Chains and Assets added via economic weight</li>
                <li>High performance CosmosSDK replicated state machine</li>
              </ul>
            </div>
          </div>
          <div className="col-lg-6 text-center my-5">
            <div className="container">
              <div className="row my-5">
                <div className="col mx-3 py-5">
                  <img className="img-fluid" src={require('assets/img/scalability.png')} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid min-h-60v pt-10vh pb-10vh" id="COMPATIBILITY">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 text-center" />
          <div className="col-lg-6 col-md-12">
            <h2 className="my-1">SOLVING CROSS-CHAIN</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 text-center mt-1">
            <div className="container">
              <div className="row">
                <div className="col mx-3 pt-5">
                  <img className="img-fluid" src={require('assets/img/thorchain-tree.png')} />
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-12">
            <div className="container pitch-text py-5">
              <ul>
                <li>THORChain observes transactions on external networks</li>
                <li>State is highly-validated: incorrect transactions are ignored or refunded</li>
                <li>Logic is applied to state changes, generating outgoing transactions</li>
                <li>Transactions are signed via a chain agnostic TSS protocol</li>
                <li>Outgoing transactions are broadcast back to the external network</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
)

export default TechSection
