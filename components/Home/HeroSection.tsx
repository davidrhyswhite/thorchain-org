import React, { FC } from 'react'



export const HeroSection: FC = () => (
  <section id="top">
    <div className="container-fluid top-div min-h-80v pb-10vh" id="hero">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-12">
            <div className="row top-text" style={{ marginTop: '100px' }}>
              <h1>A DECENTRALISED LIQUIDITY NETWORK</h1>
              <br />
              <h5>
                THORChain facilitates cross-chain liquidity pools with no pegged or wrapped tokens.
              </h5>
              <br />
              <p className="desc-paragraph">
                {' '}
                Swap between assets across chains. Stake assets to earn yield. Run a
                node to service the network.
              </p>
              <br />
            </div>
            <div className="row text-left">
              <div className="col-12" style={{ fontSize: '1.5rem' }}>
                <a href="https://twitter.com/thorchain_org" target="_blank">
                  <i className="text-grey mx-10px fab fa-twitter" />
                </a>
                <a href="https://reddit.com/r/thorchain" target="_blank">
                  <i className="text-grey mx-10px fab fa-reddit" />
                </a>
                <a href="https://gitlab.com/thorchain" target="_blank">
                  <i className="text-grey mx-10px fab fa-gitlab" />
                </a>
                <a href="https://t.me/thorchain_org" target="_blank">
                  <i className="text-grey mx-10px fab fa-telegram-plane" />
                </a>
                <a href="https://discord.gg/V6pNp8A" target="_blank">
                  <i className="text-grey mx-10px fab fa-discord" />
                </a>
                <a href="https://medium.com/thorchain" target="_blank">
                  <i className="text-grey mx-10px fab fa-medium" />
                </a>
              </div>
              <div className="col-12" style={{ marginTop: '20px' }}>
                <h5>
                  <a
                    href="https://coinmarketcap.com/currencies/thorchain/"
                    target="_blank"
                    style={{ color: '#50E3C2', fontSize: '18px' }}
                  >
                    VIEW ON COINMARKETCAP
                  </a>
                </h5>
              </div>
              <div className="col-12" style={{ marginTop: '20px' }}>
                <a href="https://testnet.bepswap.com" target="_blank">
                  <button
                    className="btn btn-thor py-2"
                    style={{ height: '45px', marginTop: '20px' }}
                  >
                    VIEW TESTNET
                  </button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default HeroSection
